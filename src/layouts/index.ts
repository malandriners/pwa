export { default as Header } from './Header.astro'
export { default as Main } from './Main.astro'
export { default as Footer } from './Footer.astro'
export { default as Layout } from './Layout.astro'

