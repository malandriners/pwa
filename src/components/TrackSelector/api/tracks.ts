 import mock from './mock.json'

const API_ROOT = 'https://www.webreactiva.com'
const API_ROUTE = '/api/podcast/episodes'

export interface TrackView {
    id: string
    title: string
    date: number
    url: string
}

const mapToTrackView = (track: any): TrackView => ({
    id: track.spreaker_episode_id,
    title: track.title,
    date: track.published_at,
    url: track.audio_file,
})


export const getTracks = async (page:number = 1): Promise<TrackView[]> => {
    let json = null
    let limit = page
    if (import.meta.env.MODE === 'production') {
        const response = await fetch(`${API_ROOT}${API_ROUTE}?page=${page}`)
        json = await response.json()
    } else {
        json = mock
        limit = page + 1000
    }

    return [
        ...json?.data.map(mapToTrackView),
        ...limit <= (json?.meta.totalPages ?? 0)
            ?  await getTracks(page + 1)
            : []
    ] || []
}
