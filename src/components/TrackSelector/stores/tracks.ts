import { Readable, Writable, derived, writable } from "svelte/store"
import { TrackView, getTracks } from "../api/tracks"

const createTrackStore = () => {

    const __items__: Writable<TrackView[]> = writable([])
    const __loading__: Writable<boolean> = writable(false)
    
    const items: Readable<TrackView[]> = derived([__items__], ([$x]) => $x)
    const loading: Readable<boolean> = derived([__loading__], ([$x]) => $x)

    const reload = async () => {
        __loading__.set(true)
        __items__.set(await getTracks())
        __loading__.set(false)
    }

    return { loading, items, reload }
}

export const trackStore = createTrackStore()
