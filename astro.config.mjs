import { defineConfig } from 'astro/config'

import svelte from '@astrojs/svelte'
import sitemap from '@astrojs/sitemap'
import compress from 'astro-compress'
import { VitePWA } from 'vite-plugin-pwa'

import { manifest, basePath, baseURL } from './util/seoConfig'

export default defineConfig({
    integrations: [sitemap(), compress(), svelte()],
    site: baseURL,
    base: basePath,
    sitemap: true,
    outDir: 'public',
    publicDir: 'static',
    vite: {
        plugins: [
            VitePWA({
                registerType: 'autoUpdate',
                manifest,
                manifestFilename: 'manifest.json',
                workbox: {
                    globDirectory: 'public',
                    globPatterns: [
                        '**/*.{js,css,svg,png,jpg,jpeg,gif,webp,woff,woff2,ttf,eot,ico}',
                    ],
                    navigateFallback: null,
                },
            }),
        ],
    },
})
