# PWA!!

### Petándolo Wapamente Abuela!!

Mire Abuela!! Mire cómo vuelo!! Como los ángeles!! [*](https://youtu.be/b5SCTck30Xc?t=187)


Y más alto que voy a volar haya comprendido los entresijos de las PWAs, gracias a este [reto](https://github.com/webreactiva-devs/reto-pwa) propuesto desde [Web Reactiva](https://www.webreactiva.com/).


### Puntos del reto

#### Primera contienda
- [X] Hacer la aplicacion web que reproduzca audios
- [X] Hacer que se pueda instalar en un dispositivo
- [X] Agregar una pantalla de inicio (splash screen)

#### Segunda contienda
por definir
#### Tercera contienda
por definir

### Resultado
[Aquí puedes probar la aplicación](https://malandriners.gitlab.io/pwa/)
