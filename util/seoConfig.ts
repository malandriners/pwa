import type { ManifestOptions } from 'vite-plugin-pwa'

const name = 'Petándolo Wapamente Abuela!'
const short_name = 'Abuela PW'
const description = 'Mire abuela! Mire cómo vuelo! Como los ángeles!'
const baseDomain = 'malandriners.gitlab.io'
export const basePath = '/pwa'
export const baseURL = `https://${baseDomain}/${basePath}/`


export const seoConfig = {
    baseURL,
    description,
    type: 'website',
    image: {
        url: `${baseURL}/screenshots/1200x1200.png`,
        // Change this to your website's thumbnail.
        alt: description,
        width: 1200,
        height: 1200,
    },
    siteName: name,
    twitter: {
        card: 'summary_large_image',
    },
}

export const manifest: Partial<ManifestOptions> = {
    name,
    short_name,
    description,
    background_color: '#fdfdfd',
    theme_color: '#db4938',
    display: 'fullscreen',
    start_url: "index.html",
    scope: "/",
    shortcuts: [
        {
            name: 'Petándolo Wapamente Abuela!',
            short_name: 'PWA!',
            description: 'Abuelita petándolo wapamente',
            url: '/',
            icons: [{ src: '/icons/192.png', sizes: '192x192' }],
        },
    ],
    screenshots: [
        {
            src: 'screenshots/1200x1200.png',
            sizes: '1200x1200',
            type: 'image/png',
        }
    ],
    icons: [
        {
            src: 'icons/72.png',
            type: 'image/png',
            sizes: '72x72',
        },
        {
            src: 'icons/96.png',
            type: 'image/png',
            sizes: '96x96',
        },
        {
            src: 'icons/128.png',
            type: 'image/png',
            sizes: '128x128',
        },
        {
            src: 'icons/144.png',
            type: 'image/png',
            sizes: '144x144',
        },
        {
            src: 'icons/152.png',
            type: 'image/png',
            sizes: '152x152',
        },
        {
            src: 'icons/192.png',
            type: 'image/png',
            sizes: '192x192',
        },
        {
            src: 'icons/384.png',
            type: 'image/png',
            sizes: '384x384',
        },
        {
            src: 'icons/512.png',
            type: 'image/png',
            sizes: '512x512',
        },
    ],
}
